# 事前準備
GOOGLE_API_KEY は Google Cloud Platform にて、Geocoding API のアクセスを許可したAPIキーを入力してください。
map.pyのjapan.gegojson の参照は、[こちら](https://github.com/dataofjapan/land/blob/master/japan.geojson "github")をダウンロードして、geoディレクトリに配置してください。

# 使い方
## スクレイピング
main/scrape.py で情報を取得します。
time.sleep(3) はサイトに負荷をかけないために必要です。

## 緯度・経度取得
main/position.py で、緯度経度情報を取得します。
Google Geocoding API を使って店舗名から、緯度経度を取得します。
1000件/月　以上で料金がかかるため、トップ100とワースト100に限定していますが、お好みで変更してください。

## マッピング
main/map.py で、日本地図に店舗情報をマッピングします。
以下の参照ファイル名を変えれば、時給と（時給 - 最低賃金）を切り替えられます。
```python:main/map.py
top_file = open('./data/diff_top.json', 'r')
```

## ヒストグラム
main/histgram.py で、ヒストグラムを作成します。