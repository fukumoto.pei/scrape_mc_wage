import requests
import re
import time
import json
from bs4 import BeautifulSoup

HOST = 'https://baito.mynavi.jp'
BASE_URL = f'{HOST}/brand/brand-12' # area_46/

minimum_wages = {
    'area_1': 861,
    'area_2': 793,
    'area_3': 793,
    'area_4': 825,
    'area_5': 792,
    'area_6': 793,
    'area_7': 800,
    'area_8': 851,
    'area_9': 854,
    'area_10': 837,
    'area_11': 928,
    'area_12': 925,
    'area_13': 1013,
    'area_14': 1012,
    'area_15': 831,
    'area_16': 849,
    'area_17': 833,
    'area_18': 830,
    'area_19': 838,
    'area_20': 849,
    'area_21': 852,
    'area_22': 885,
    'area_23': 927,
    'area_24': 874,
    'area_25': 868,
    'area_26': 909,
    'area_27': 964,
    'area_28': 900,
    'area_29': 838,
    'area_30': 831,
    'area_31': 792,
    'area_32': 792,
    'area_33': 834,
    'area_34': 871,
    'area_35': 829,
    'area_36': 796,
    'area_37': 820,
    'area_38': 793,
    'area_39': 792,
    'area_40': 842,
    'area_41': 792,
    'area_42': 793,
    'area_43': 793,
    'area_43': 793,
    'area_44': 792,
    'area_45': 793,
    'area_46': 793,
    'area_47': 792,
}

def scraper():
    data = []
    for i in range(47):
        print('エリア: ', i + 1)
        response = requests.get(f'{BASE_URL}/area_{i + 1}/?pageNo=1')
        soup = BeautifulSoup(response.text, 'html.parser')
        for k in range(int(soup.select_one('.jobOfferSearchPagerInner ul.pager li:last-child a').get_text())):
            response = requests.get(f'{BASE_URL}/area_{i + 1}/?pageNo={k + 1}')
            soup = BeautifulSoup(response.text, 'html.parser')
            for section in soup.select('div.jobOfferSearchListWrap section'):
                shop_name = section.select_one('.shopName').get_text()
                pos = shop_name.find('※')
                if pos > 0:
                    shop_name = shop_name[:pos] # Ex. マクドナルド 南新川店　※2021年9月16日OPEN
                wages = re.findall(r"\d+", section.select_one('.baseInformationContentNotEllipsis').get_text())
                wage = '0'
                w_i = 0
                while (len(wage) < 3): # Ex.（17時〜18時）時給900円～　＋交通費
                    wage = wages[w_i]
                    w_i = w_i + 1
                    if w_i == len(wages):
                        break

                wage = int(wage)
                diff = wage - minimum_wages[f'area_{i + 1}']
                
                data.append({
                    'shop_name': shop_name,
                    'wage': wage,
                    'diff': diff,
                })
                print(shop_name, wage, diff)

            time.sleep(3)

    data = sorted(data, key=lambda x: x['wage'])

    save_dict(data)

def save_dict(list):
    with open('data/info.json', 'w') as f:
        json.dump(list, f, indent=4, ensure_ascii=False)

if __name__ == '__main__':
    scraper()