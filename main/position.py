import json
import requests

GOOGLE_API_KEY = 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'

def main():
    json_file = open('./data/info.json', 'r')
    json_data = json.load(json_file)

    NUM = 100

    top = json_data[(-1 * NUM):]
    worst = json_data[:NUM]

    diff_data = sorted(json_data, key=lambda x: x['diff'])
    diff_top = diff_data[(-1 * NUM):]
    diff_worst = diff_data[:NUM]

    print('worst', len(worst))
    print('top', len(top))

    diff_top_data = []

    for data in diff_top:
        diff_top_data.append(fetch_location(data))

    save_dict(diff_top_data, 'diff_top')

    diff_worst_data = []

    for data in diff_worst:
        diff_worst_data.append(fetch_location(data))

    save_dict(diff_worst_data, 'diff_worst')

def fetch_location(data):
    response = requests.get(f'https://maps.googleapis.com/maps/api/geocode/json?address={data["shop_name"]}&key={GOOGLE_API_KEY}')
    json_res = json.loads(response.content)
    print('geo', type(json_res), json_res['results'][0]['geometry']['location'])

    return {
        **data,
        'location': json_res['results'][0]['geometry']['location']
    }

def save_dict(list, file_name):
    with open(f'data/{file_name}.json', 'w') as f:
        json.dump(list, f, indent=4, ensure_ascii=False)

if __name__ == '__main__':
    main()