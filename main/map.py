import json
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt

SIZE = 20
ALPHA = 0.5

def main():
    df = gpd.read_file('../geo/japan.geojson')
    top_file = open('./data/diff_top.json', 'r')
    top_data = json.load(top_file)
    worst_file = open('./data/diff_worst.json', 'r')
    worst_data = json.load(worst_file)

    with plt.style.context("ggplot"):
        df.plot(figsize=(8, 8), edgecolor='#444', facecolor='gray', linewidth = 0.5)
        plt.xlim([125, 150])
        plt.ylim([25.29, 45.90])

        for data in top_data:
            plt.scatter(data['location']['lng'], data['location']['lat'], marker='o', s=SIZE, color="orange", alpha=ALPHA, linewidths = 2)

        for data in worst_data:
            plt.scatter(data['location']['lng'], data['location']['lat'], marker='o', s=SIZE, color="green", alpha=ALPHA, linewidths = 2)

    plt.show()

if __name__ == '__main__':
    main()