import matplotlib.pyplot as plt
import json

def main():
    json_file = open('./data/info.json', 'r')
    json_data = json.load(json_file)

    diffs = []

    for data in json_data:
        diffs.append(data['diff'])
    
    print('json_data', len(json_data))

    plt.hist(diffs, color='gray', bins=[0, 50, 100, 150, 200, 250, 300, 350, 400])
    plt.show()

if __name__ == '__main__':
    main()